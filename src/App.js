import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {
  BrowserRouter,
    Switch,
    Route,
    Link
} from 'react-router-dom';

import Page1 from './component/Page1'
import Page2 from './component/Page2'
import Page3 from './component/Page3'

function App() {
return (<BrowserRouter>
      <div>
        <ul>
     <li>
 <Link to="/">Page1</Link>
     </li>
          <li>
 <Link to="/page-2/coucou">Page2</Link>
     </li>
          <li>
  <Link to="/page-3">Page3</Link>
     </li>

        </ul>

        <Switch>

          <Route exact path = "/">
            <Page1/>
          </Route>
  <Route  path = "/page-2/:id">
            <Page2/>
          </Route>
  <Route path = "/page-3">
            <Page3/>
          </Route>

        </Switch>
      </div>
</BrowserRouter>
)
}

export default App;

