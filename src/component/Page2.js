import React from 'react';
import {useParams} from 'react-router-dom';

export default function Page2() {
  let {id}= useParams();
  return <h1> Page 2 : {id} </h1>;
}
