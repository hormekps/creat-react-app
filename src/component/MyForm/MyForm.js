import React from 'react';
import axios from 'axios';

const api = 'http://localhost:3015/api/hostels/';

export  default function MyForm({handleUser}) {
  const [uid, setUid] = React.useState('');

  React.useEffect(() => {
    async function getUserByUid(uid) {
      if (!uid) {
        handleUser({user: null});
        return;
      }
      handleUser('...loading');
      const newUser = await axios.get(api + uid);
      handleUser(newUser);
    }
    getUserByUid(uid);
  }, [uid]);

  function handleInput(event) {
    const {value} = event.target;
    setUid(value);
  }

  return <form>
    <label htmlFor="uid">Uid</label>
    <input
        value={uid}
        onChange={handleInput}
        type="text" id="uid"/>
  </form>;
}
